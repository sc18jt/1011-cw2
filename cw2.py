## Introductory Python Programming
## Coursework 2
## Brandon Bennett

## To complete this coursework, you must replace the line "pass"
## in the following function definitions, with code that correctly
## computes the required result from given input parameters, according
## to the specification given on the coursework web-site.

coins = [2.0,1.0,0.50,0.20,0.10,0.05,0.02,0.01]
dictionary  = open("english_words.txt", "r")
## Q1
## Test whether string1 is an anagram of string2.
## Return a Boolean value (True or False)
def anagrams( string1, string2 ):
    if len(string1) == len(string2):

        string1 = string1.lower()
        string2 = string2.lower()
        
        for i in range(0, len(string1)):
            if string1[i] in string2:
                pass
            else:
                return False
        return True
    else:
        return False

## Q2
## Test whether string is an English word.
## Return true
def is_english_word( string ):
    
    for line in dictionary:
        if (string.lower() in line):
            return True
            break
        else:
            pass

    return False
    

## Q3
def find_all_anagrams( string ):
    
    ana_list = []
    for line in dictionary:
        if string != line.strip() and anagrams(string, line.strip()):
            ana_list.append(line.strip())
        else:
            pass

    return ana_list


## Q4
## Assess the strength of a password according to given rules.
def password_strength( string ):
    if (len(string) < 8) or (is_english_word(string)):
        return "WEAK"
    elif len(string) >= 11:
        test = [False,False,False]
        for letter in string:
            if test[0] == False and letter == letter.lower():
                test[0] = True
            elif test[1] == False and letter.isdigit() == False and letter == letter.upper():
                test[1] = True
            elif test[2] == False and letter.isdigit():
                test[2] = True
        if test[0] == True and test[1]== True and test[2] == True:
            return "STRONG"
        else:
            return "MEDIUM"
    else:
        return "MEDIUM"


## Q5
## Return a list with the mumbers of each denomination of British coins
## required to pay a given amount.
def pay_with_coins( amount ):
    count = [0,0,0,0,0,0,0,0,]

    while amount > 0:
        for c in coins:
            amount = round(amount, 2)
            if amount >= c:
                count[coins.index(c)] = count[coins.index(c)] + 1
                amount -= c
                break
            else:
                pass
    return count


## Q6
## Find a way of paying a certain amount from a given "pocket" of coins.
def pay_with_coins_from_pocket( amount, pocket ):
    global coins
    count = pay_with_coins(amount)
    paid = [0,0,0,0,0,0,0,0,0]
    total = 0

    i = 0
    for c in pocket:
        total = total + (coins[i]*c)
        total = round(total, 2)
        i = i + 1
                         
    
    if total < amount:
        return False

    else:
        paid[8] = round(total - amount, 2)
        while round(amount, 2) > 0:
            for c in coins:
                amount = round(amount, 2)
                if amount >= c and pocket[coins.index(c)] > 0:
                    amount -= c
                    paid[coins.index(c)] = paid[coins.index(c)] + 1
                    break
                else:
                    pass
    
    return paid